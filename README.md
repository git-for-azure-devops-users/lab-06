# Git for TFS users
Lab 06: Using rebase to merge feature branches

---

# Tasks

 - Clone and inspect the repository
 
 - Rebase "feature/b" into "feature/a"
 
 - Inspect the repository history

---

## Clone and inspect the repository

 - Clone the lab repository from visual studio:

```
http://<server-ip>:8080/tfs/DefaultCollection/git-workshop/_git/demo-app-lab-06
```

- Checkout the branch "feature/a" and review the history by clicking "View History"

&nbsp;
<img alt="Image 1.1" src="https://docs.microsoft.com/en-us/azure/devops/repos/git/_img/history/team-explorer-changes.png?view=azure-devops" border="1">
&nbsp;

- Checkout the branch "feature/b" and review the history

---

## Rebase "feature/b" into "feature/a"

 - Rebase the branch "feature/b" into "feature/a" by click "Rebase" from the "Branches" view

&nbsp;
<img alt="Image 1.1" src="https://msdnshared.blob.core.windows.net/media/2017/10/image_thumb36.png" border="1">
&nbsp;

 - Resolve the merge conflict by click "Conflicts: 1", select the file and click "Merge"

&nbsp;
<img alt="Image 1.1" src="https://msdnshared.blob.core.windows.net/media/2017/10/image_thumb39.png" border="1">
&nbsp;

 - Resolve the merge conflict by click "Accept Merge" and commit the changes

---

## Inspect the repository history

 - Checkout the branch "feature/b" and review the history

 - Browse to the web portal and check the history from the commits view
